package com.gokisoft.c1907l.lesson09;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gokisoft.c1907l.R;
import com.gokisoft.c1907l.config.Config;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import androidx.appcompat.app.AppCompatActivity;

public class DownloadActivity2 extends AppCompatActivity implements View.OnClickListener{
    TextView titleView, titleView2;
    ProgressBar progressBar, progressBar2;
    Button startBtn, stopBtn, startBtn2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download2);

        titleView = findViewById(R.id.ad_title);
        progressBar = findViewById(R.id.ad_progressbar);
        startBtn = findViewById(R.id.ad_start_service_btn);
        stopBtn = findViewById(R.id.ad_stop_service_btn);
        titleView2 = findViewById(R.id.ad_title2);
        progressBar2 = findViewById(R.id.ad_progressbar2);
        startBtn2 = findViewById(R.id.ad_start_service_btn2);

        startBtn.setOnClickListener(this);
        startBtn2.setOnClickListener(this);
        stopBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.equals(startBtn)) {
            startBtn.setVisibility(View.GONE);
            //Start Download
//            startDownloadByThread(Config.YOUTUBE_URL);
            startDownloadByAsync(Config.YOUTUBE_URL);
        } else if(v.equals(startBtn2)) {
            startBtn2.setVisibility(View.GONE);
//            startDownloadByThread(Config.YOUTUBE_URL2);
            startDownloadByAsync(Config.YOUTUBE_URL2);
        } else if(v.equals(stopBtn)) {
            //Stop Download
            finish();
        }
    }

    private void startDownloadByThread(final String videoUrl) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                downloadFile(videoUrl);
            }
        }).start();
    }

    private void startDownloadByAsync(String videoUrl) {
        DownloadAsyncTask asyncTask = new DownloadAsyncTask();
        asyncTask.execute(videoUrl);
    }

    class DownloadAsyncTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            //Khoi tao du lieu.
        }

        @Override
        protected String doInBackground(String... params) {
            String url = params[0];

            downloadFile(url);
            return null;
        }

        @Override
        protected void onPostExecute(String o) {
            //Luc ket thuc xu ly cua async
            //Giai phong tai nguyen

        }
    }

    private void downloadFile(String videoUrl) {
        InputStream input = null;
        OutputStream output = null;

        int count;
        try {
            URL url = new URL(videoUrl);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream
            output = new FileOutputStream(Environment
                    .getExternalStorageDirectory().toString()
                    + File.separator
                    + Environment.DIRECTORY_MOVIES
                    + File.separator
                    + videoUrl.hashCode() + ".mp4");

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                int percent = (int) ((total * 100) / lenghtOfFile);

                if(videoUrl.equals(Config.YOUTUBE_URL)) {
                    updateUI(1, percent);
                } else {
                    updateUI(2, percent);
                }

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // closing streams
            if(output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    int lastPercent1 = 0;
    int lastPercent2 = 0;
    void updateUI(final int position, final int percent) {
        if(position == 1) {
            if(percent == lastPercent1) return;
            lastPercent1 = percent;
        } else {
            if(percent == lastPercent2) return;
            lastPercent2 = percent;
        }

        Log.d(DownloadActivity2.class.getName(), "percent: " + percent + ", position: " + position);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(position == 1) {
                    titleView.setText("Loading " + percent + "% ...");
                    progressBar.setProgress(percent);
                } else {
                    titleView2.setText("Loading " + percent + "% ...");
                    progressBar2.setProgress(percent);
                }
            }
        });
    }
}
