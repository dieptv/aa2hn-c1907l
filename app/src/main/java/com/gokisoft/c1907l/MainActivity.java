package com.gokisoft.c1907l;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gokisoft.c1907l.config.Config;
import com.gokisoft.c1907l.models.Book;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    EditText bookNameTxt, authorNameTxt, priceTxt, descriptionTxt;
    Button saveBtn, resetBtn, bookDetailBtn;

    Book book = new Book();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Mapping UI -> Object trong Java
        bookNameTxt = findViewById(R.id.am_book_name);
        authorNameTxt = findViewById(R.id.am_author_name);
        priceTxt = findViewById(R.id.am_price);
        descriptionTxt = findViewById(R.id.am_description);
        saveBtn = findViewById(R.id.am_save_btn);
        resetBtn = findViewById(R.id.am_reset_btn);
        bookDetailBtn = findViewById(R.id.am_book_detail_btn);

        String bookName = getIntent().getStringExtra("bookName");
        String authorName = getIntent().getStringExtra("authorName");
        int price = getIntent().getIntExtra("price", 0);
        String description = getIntent().getStringExtra("description");

        bookNameTxt.setText(bookName);
        authorNameTxt.setText(authorName);
        descriptionTxt.setText(description);
        priceTxt.setText(""+price);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Khi nguoi dung click day -> xu ly.
                Log.d(MainActivity.class.getName(), "click save ...");
                String bookName = bookNameTxt.getText().toString();
                String authorName = authorNameTxt.getText().toString();
                int price = Integer.parseInt(priceTxt.getText().toString());
                String description = descriptionTxt.getText().toString();

                book = new Book(bookName, authorName, description, price);

                Log.d(MainActivity.class.getName(), book.toString());

                Toast.makeText(MainActivity.this, bookName, Toast.LENGTH_SHORT).show();

                Intent intent = new Intent();
                intent.putExtra("bookName", bookName);
                intent.putExtra("authorName", authorName);
                intent.putExtra("price", price);
                intent.putExtra("description", description);

                setResult(Config.REQUEST_CODE_UPDATE_BOOK, intent);

                finish();
            }
        });

        resetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookNameTxt.setText("");
                authorNameTxt.setText("");
                priceTxt.setText("");
                descriptionTxt.setText("");
            }
        });

        bookDetailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, BookDetailActivity.class);
                i.putExtra("bookName", bookNameTxt.getText().toString());

                startActivity(i);
            }
        });
    }
}
