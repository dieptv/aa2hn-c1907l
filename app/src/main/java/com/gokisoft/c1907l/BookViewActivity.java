package com.gokisoft.c1907l;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.gokisoft.c1907l.config.Config;
import com.gokisoft.c1907l.models.Book;

import androidx.appcompat.app.AppCompatActivity;

public class BookViewActivity extends AppCompatActivity implements View.OnClickListener{
    TextView bookNameView, authorNameView, priceView, desView;
    Button updateBtn;

    Book book = new Book();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_view);

        //Mapping
        bookNameView = findViewById(R.id.abv_book_name);
        authorNameView = findViewById(R.id.abv_author_name);
        priceView = findViewById(R.id.abv_price);
        desView = findViewById(R.id.abv_description);

        updateBtn = findViewById(R.id.abv_update);

        /**updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Bat su kien -> click vao button update
            }
        });*/
        updateBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.equals(updateBtn)) {
            //Khi nguoi dung click button update
            Intent i = new Intent(this, MainActivity.class);

            i.putExtra("bookName", book.getBookName());
            i.putExtra("authorName", book.getAuthorName());
            i.putExtra("price", book.getPrice());
            i.putExtra("description", book.getDescription());

            startActivityForResult(i, Config.REQUEST_CODE_UPDATE_BOOK);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Config.REQUEST_CODE_UPDATE_BOOK:
                String bookName = data.getStringExtra("bookName");
                String authorName = data.getStringExtra("authorName");
                int price = data.getIntExtra("price", 0);
                String description = data.getStringExtra("description");

                book.setBookName(bookName);
                book.setAuthorName(authorName);
                book.setPrice(price);
                book.setDescription(description);

                //Update du lieu len view
                bookNameView.setText(bookName);
                authorNameView.setText(authorName);
                desView.setText(description);
                priceView.setText(""+price);
                break;
        }
    }
}
