package com.gokisoft.c1907l;

import android.os.Bundle;

import com.gokisoft.c1907l.adapter.ComicRecycleAdapter;
import com.gokisoft.c1907l.models.Comic;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class RecycleComicActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    List<Comic> dataList;
    ComicRecycleAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycle_comic);

        recyclerView = findViewById(R.id.arc_recycleview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //Fake du lieu
        dataList = new ArrayList<>();
        dataList.add(new Comic("https://i.chzbgr.com/full/9175388928/hCC2E3E2C/funny-comic-cartoon-choose-i-choose-you-your-first-pokemon-oak-oak-sephko", "Truyen 1", "Noi Dung 1"));
        dataList.add(new Comic("https://static.vecteezy.com/system/resources/thumbnails/000/660/520/small/Laugh_Design.jpg", "Truyen 2", "Noi Dung 2"));
        dataList.add(new Comic("https://i.pinimg.com/originals/52/ee/43/52ee437272c57b92351eda4156eca703.jpg", "Truyen 3", "Noi Dung 3"));

        adapter = new ComicRecycleAdapter(this, dataList);


        recyclerView.setAdapter(adapter);
    }
}
