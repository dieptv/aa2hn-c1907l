package com.gokisoft.c1907l.models;

/**
 * Created by Diep.Tran on 8/18/21.
 */

public class Book {
    String bookName, authorName, description;
    int price;

    public Book() {
    }

    public Book(String bookName, String authorName, String description, int price) {
        this.bookName = bookName;
        this.authorName = authorName;
        this.description = description;
        this.price = price;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookName='" + bookName + '\'' +
                ", authorName='" + authorName + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                '}';
    }
}
