package com.gokisoft.c1907l.models;

/**
 * Singleton
 * Created by Diep.Tran on 8/20/21.
 */

public class DataController {
    Book book;

    private static DataController instance = null;

    private DataController() {
        book = new Book();
    }

    public synchronized static DataController getInstance() {
        if(instance == null) {
            instance = new DataController();
        }
        return instance;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
