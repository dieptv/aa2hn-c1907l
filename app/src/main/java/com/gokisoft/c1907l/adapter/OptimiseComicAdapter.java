package com.gokisoft.c1907l.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gokisoft.c1907l.R;
import com.gokisoft.c1907l.models.Comic;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Diep.Tran on 8/25/21.
 */

public class OptimiseComicAdapter extends BaseAdapter{
    Activity activity;
    List<Comic> dataList;

    public OptimiseComicAdapter(Activity activity, List<Comic> dataList) {
        this.activity = activity;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    static int count = 0;
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ComicHolder holder;

        if(convertView == null) {
            Log.d(ComicAdapter.class.getName(), "count: " + ++count);

            convertView = activity.getLayoutInflater().inflate(R.layout.comic_item, null);

            ImageView thumbnailView = convertView.findViewById(R.id.ci_thumbnail);
            TextView titleView = convertView.findViewById(R.id.ci_title);
            TextView desView = convertView.findViewById(R.id.ci_description);

            holder = new ComicHolder(thumbnailView, titleView, desView);

            convertView.setTag(holder);
        } else {
            holder = (ComicHolder) convertView.getTag();
        }

        //thumbnailView.setImageResource(R.drawable.thumbnail);
        Comic comic = dataList.get(position);

        holder.titleView.setText(comic.getTitle());
        holder.desView.setText(comic.getDescription());
        Picasso.with(activity).load(comic.getThumbnail()).into(holder.thumbnailView);

        return convertView;
    }

    class ComicHolder {
        ImageView thumbnailView;
        TextView titleView;
        TextView desView;

        public ComicHolder(ImageView thumbnailView, TextView titleView, TextView desView) {
            this.thumbnailView = thumbnailView;
            this.titleView = titleView;
            this.desView = desView;
        }
    }
}
