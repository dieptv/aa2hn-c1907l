package com.gokisoft.c1907l.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gokisoft.c1907l.R;
import com.gokisoft.c1907l.models.Comic;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Diep.Tran on 8/25/21.
 */

public class ComicRecycleAdapter extends RecyclerView.Adapter<ComicRecycleAdapter.ComicRecycleHolder>{
    Activity activity;
    List<Comic> dataList;

    public ComicRecycleAdapter(Activity activity, List<Comic> dataList) {
        this.activity = activity;
        this.dataList = dataList;
    }

    @Override
    public ComicRecycleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = activity.getLayoutInflater().inflate(R.layout.comic_item, null);
        ImageView thumbnailView = convertView.findViewById(R.id.ci_thumbnail);
        TextView titleView = convertView.findViewById(R.id.ci_title);
        TextView desView = convertView.findViewById(R.id.ci_description);

        return new ComicRecycleHolder(convertView, thumbnailView, titleView, desView);
    }

    @Override
    public void onBindViewHolder(ComicRecycleHolder holder, int position) {
        Comic c = dataList.get(position);

        holder.titleView.setText(c.getTitle());
        holder.desView.setText(c.getDescription());
        Picasso.with(activity).load(c.getThumbnail()).into(holder.thumbnailView);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class ComicRecycleHolder extends RecyclerView.ViewHolder {
        ImageView thumbnailView;
        TextView titleView;
        TextView desView;

        public ComicRecycleHolder(View itemView, ImageView thumbnailView, TextView titleView, TextView desView) {
            super(itemView);
            this.thumbnailView = thumbnailView;
            this.titleView = titleView;
            this.desView = desView;
        }
    }
}
