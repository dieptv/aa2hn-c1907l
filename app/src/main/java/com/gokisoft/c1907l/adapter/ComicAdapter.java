package com.gokisoft.c1907l.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gokisoft.c1907l.R;
import com.gokisoft.c1907l.models.Comic;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Diep.Tran on 8/25/21.
 */

public class ComicAdapter extends BaseAdapter{
    Activity activity;
    List<Comic> dataList;

    public ComicAdapter(Activity activity, List<Comic> dataList) {
        this.activity = activity;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    static int count = 0;
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d(ComicAdapter.class.getName(), "count: " + ++count);

        convertView = activity.getLayoutInflater().inflate(R.layout.comic_item, null);

        ImageView thumbnailView = convertView.findViewById(R.id.ci_thumbnail);
        TextView titleView = convertView.findViewById(R.id.ci_title);
        TextView desView = convertView.findViewById(R.id.ci_description);

        //thumbnailView.setImageResource(R.drawable.thumbnail);
        Comic comic = dataList.get(position);

        titleView.setText(comic.getTitle());
        desView.setText(comic.getDescription());
        Picasso.with(activity).load(comic.getThumbnail()).into(thumbnailView);

        return convertView;
    }
}
