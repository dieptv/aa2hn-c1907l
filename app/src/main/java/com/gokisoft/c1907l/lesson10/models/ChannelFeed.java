package com.gokisoft.c1907l.lesson10.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Diep.Tran on 9/13/21.
 */

@Root(name = "channel")
public class ChannelFeed {
    @Element
    public String title;

    @Element
    public String description;

    @Element(name = "image")
    public ImageFeed imageFeed;

    @Element(name = "pubDate")
    public String pubDate;

    @Element(name = "generator")
    public String generator;

    @Element(name = "link")
    public String link;

    @ElementList(name = "item", inline = true, required = false)
    public List<ItemFeed> itemList;
}
