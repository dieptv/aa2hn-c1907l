package com.gokisoft.c1907l.lesson10.models;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Diep.Tran on 9/13/21.
 */

public interface RssService {
    int RSS_COMIC = 1;
    int RSS_LASTEST = 2;
    int RSS_GLOBAL = 3;
    int RSS_NEWS = 4;
    int RSS_HEALTHY = 5;

    @GET("rss/cuoi.rss")
    Call<RssFeed> getComicFeed();

    @GET("rss/tin-moi-nhat.rss")
    Call<RssFeed> getLastestFeed();

    @GET("rss/the-gioi.rss")
    Call<RssFeed> getGlobalFeed();

    @GET("rss/thoi-su.rss")
    Call<RssFeed> getNewsFeed();

    @GET("rss/suc-khoe.rss")
    Call<RssFeed> getHealthyFeed();
}
