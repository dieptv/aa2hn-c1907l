package com.gokisoft.c1907l.lesson10.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Diep.Tran on 9/13/21.
 */

@Root(name = "image")
public class ImageFeed {
    @Element(name = "url")
    public String url;

    @Element(name = "title")
    public String title;

    @Element(name = "link")
    public String link;
}
