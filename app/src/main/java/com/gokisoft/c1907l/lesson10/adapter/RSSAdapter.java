package com.gokisoft.c1907l.lesson10.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gokisoft.c1907l.R;
import com.gokisoft.c1907l.lesson10.models.ItemFeed;

import java.util.List;

/**
 * Created by Diep.Tran on 9/13/21.
 */

public class RSSAdapter extends BaseAdapter{
    Activity activity;
    List<ItemFeed> dataList;

    public RSSAdapter(Activity activity, List<ItemFeed> dataList) {
        this.activity = activity;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = activity.getLayoutInflater().inflate(R.layout.rss_item, null);
        TextView titleView = convertView.findViewById(R.id.ri_title);
        TextView desView = convertView.findViewById(R.id.ri_description);

        ItemFeed itemFeed = dataList.get(position);

        titleView.setText(itemFeed.title);
        desView.setText(itemFeed.description);

        return convertView;
    }
}
