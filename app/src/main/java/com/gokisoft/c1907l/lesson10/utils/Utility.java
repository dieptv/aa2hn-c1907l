package com.gokisoft.c1907l.lesson10.utils;

import com.gokisoft.c1907l.lesson10.models.ILoadSuccess;
import com.gokisoft.c1907l.lesson10.models.ItemFeed;
import com.gokisoft.c1907l.lesson10.models.RssFeed;
import com.gokisoft.c1907l.lesson10.models.RssService;

import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Created by Diep.Tran on 9/13/21.
 */

public class Utility {
    static final String BASE_URL = "https://vnexpress.net/";

    public static void loadRssFeed(final ILoadSuccess iLoadSuccess, int type) {
        Retrofit.Builder builder = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(SimpleXmlConverterFactory.create());

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        builder.client(httpClient.build());

        Retrofit retrofit = builder.build();

        RssService rssService = retrofit.create(RssService.class);

        Call<RssFeed> callAsync;

        switch (type) {
            case RssService.RSS_COMIC:
                callAsync = rssService.getComicFeed();
                break;
            case RssService.RSS_LASTEST:
                callAsync = rssService.getLastestFeed();
                break;
            case RssService.RSS_GLOBAL:
                callAsync = rssService.getGlobalFeed();
                break;
            case RssService.RSS_HEALTHY:
                callAsync = rssService.getHealthyFeed();
                break;
            case RssService.RSS_NEWS:
            default:
                callAsync = rssService.getNewsFeed();
                break;
        }

        callAsync.enqueue(new Callback<RssFeed>() {
            @Override
            public void onResponse(Call<RssFeed> call, Response<RssFeed> response) {
                if (response.isSuccessful()) {
                    RssFeed apiResponse = response.body();
                    List<ItemFeed> itemList = apiResponse.channelFeed.itemList;

                    for (ItemFeed item : itemList) {
                        item.description = item.description.replaceAll("\\<[^>]*>","");
                    }

                    iLoadSuccess.onLoadSuccess(itemList);
                } else {
                    System.out.println("Request Error :: " + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<RssFeed> call, Throwable t) {
                if (call.isCanceled()) {
                    System.out.println("Call was cancelled forcefully");
                } else {
                    System.out.println("Network Error :: " + t.getLocalizedMessage());
                }
            }
        });
    }
}
