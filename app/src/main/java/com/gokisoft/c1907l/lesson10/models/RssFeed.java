package com.gokisoft.c1907l.lesson10.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Diep.Tran on 9/13/21.
 */

@Root(name = "rss", strict = false)
public class RssFeed {
    @Element(name = "channel")
    public ChannelFeed channelFeed;
}
