package com.gokisoft.c1907l.lesson10.models;

import java.util.List;

/**
 * Created by Diep.Tran on 9/13/21.
 */

public interface ILoadSuccess {
    void onLoadSuccess(List<ItemFeed> dataList);
}
