package com.gokisoft.c1907l.lesson10.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Diep.Tran on 9/13/21.
 */

@Root(name = "item")
public class ItemFeed {
    @Element(name = "title")
    public String title;

    @Element(name = "description")
    public String description;

    @Element(name = "pubDate")
    public String pubDate;

    @Element(name = "link")
    public String link;

    @Element(name = "guid")
    public String guid;

    @Element(name = "comments")
    public int comments;

    @Override
    public String toString() {
        return "ItemFeed{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", pubDate='" + pubDate + '\'' +
                ", link='" + link + '\'' +
                ", guid='" + guid + '\'' +
                ", comments=" + comments +
                '}';
    }
}
