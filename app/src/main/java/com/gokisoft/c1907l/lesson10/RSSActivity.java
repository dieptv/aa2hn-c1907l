package com.gokisoft.c1907l.lesson10;

import android.os.Bundle;
import android.widget.ListView;

import com.gokisoft.c1907l.R;
import com.gokisoft.c1907l.lesson10.adapter.RSSAdapter;
import com.gokisoft.c1907l.lesson10.models.ILoadSuccess;
import com.gokisoft.c1907l.lesson10.models.ItemFeed;
import com.gokisoft.c1907l.lesson10.models.RssService;
import com.gokisoft.c1907l.lesson10.utils.Utility;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;

public class RSSActivity extends AppCompatActivity implements ILoadSuccess{
    ListView listView;
    List<ItemFeed> dataList;

    RSSAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rss);

        listView = findViewById(R.id.ar_listview);

        dataList = new ArrayList<>();

        adapter = new RSSAdapter(this, dataList);
        listView.setAdapter(adapter);

        Utility.loadRssFeed(this, RssService.RSS_COMIC);
    }

    @Override
    public void onLoadSuccess(List<ItemFeed> dataList) {
        for (ItemFeed item : dataList) {
            this.dataList.add(item);
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }
}
