package com.gokisoft.c1907l.comic.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.gokisoft.c1907l.comic.db.DBHelper;

/**
 * Created by Diep.Tran on 8/30/21.
 */

public class ComicModify {
    public static final String TABLE_COMIC = "comics";
    public static final String SQL_CREATE_TABLE = "create table comics (\n" +
            "\t_id integer primary key autoincrement,\n" +
            "\ttitle varchar(100),\n" +
            "\tthumbnail varchar(500),\n" +
            "\tdescription text\n" +
            ")";

    public static void save(Comic comic) {
        if(comic.getId() > 0) {
            update(comic);
        } else {
            insert(comic);
        }
    }

    /**
     * insert -> id -> null (0) -> default
     * @param comic
     */
    public static void insert(Comic comic) {
        //Lay dc doi tuong database -> thao tac du lieu.
        SQLiteDatabase db = DBHelper.getInstance(null).getWritableDatabase();

        //Chen du lieu vao database
        ContentValues values = new ContentValues();
        values.put("title", comic.getTitle());
        values.put("thumbnail", comic.getThumbnail());
        values.put("description", comic.getDescription());

        //Thuc hien truy van insert
        db.insert(TABLE_COMIC, null, values);
    }

    /**
     * update -> id (exists) > 0
     * @param comic
     */
    public static void update(Comic comic) {
        //Lay dc doi tuong database -> thao tac du lieu.
        SQLiteDatabase db = DBHelper.getInstance(null).getWritableDatabase();

        //Chen du lieu vao database
        ContentValues values = new ContentValues();
        values.put("title", comic.getTitle());
        values.put("thumbnail", comic.getThumbnail());
        values.put("description", comic.getDescription());

        //Thuc hien truy van insert
        db.update(TABLE_COMIC, values, "_id = " + comic.getId(), null);
    }

    public static void delete(int id) {
        //Lay dc doi tuong database -> thao tac du lieu.
        SQLiteDatabase db = DBHelper.getInstance(null).getWritableDatabase();

        db.delete(TABLE_COMIC, "_id = " + id, null);
    }

    public static Cursor getComicCursor() {
        SQLiteDatabase db = DBHelper.getInstance(null).getReadableDatabase();

        String sql = "select * from " + TABLE_COMIC;

        return db.rawQuery(sql, null);
    }

    public static Comic find(Cursor cursor) {
        return new Comic(
                cursor.getInt(cursor.getColumnIndex("_id")),
                cursor.getString(cursor.getColumnIndex("title")),
                cursor.getString(cursor.getColumnIndex("thumbnail")),
                cursor.getString(cursor.getColumnIndex("description"))
        );
    }
}
