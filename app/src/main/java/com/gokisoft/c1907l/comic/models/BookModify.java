package com.gokisoft.c1907l.comic.models;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.gokisoft.c1907l.comic.db.DBHelper;

/**
 * Created by Diep.Tran on 9/1/21.
 */

public class BookModify {
    public static final String TABLE_NAME = "Book";
    public static final String SQL_CREATE_TABLE = "create table if not exists Book (\n" +
            "\t_id integer primary key autoincrement,\n" +
            "\tname varchar(100),\n" +
            "\tprice integer,\n" +
            "\tauthor_name varchar(100)\n" +
            ")";

    public static void insert(Book book) {
        ContentValues values = new ContentValues();
        values.put("name", book.getName());
        values.put("price", book.getPrice());
        values.put("author_name", book.getAuthorName());

        SQLiteDatabase db = DBHelper.getInstance(null).getWritableDatabase();
        db.insert(TABLE_NAME, null, values);
    }
}
