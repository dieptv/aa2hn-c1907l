package com.gokisoft.c1907l.comic;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.gokisoft.c1907l.R;
import com.gokisoft.c1907l.comic.adapter.ComicAdapter;
import com.gokisoft.c1907l.comic.db.DBHelper;
import com.gokisoft.c1907l.comic.models.Book;
import com.gokisoft.c1907l.comic.models.BookModify;
import com.gokisoft.c1907l.comic.models.Comic;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class Comic2Activity extends AppCompatActivity {
    static final int PERMISSION_REQUEST_CODE = 1;

    ListView listView;
    List<Comic> dataList;//dataList -> json -> save -> SharedPreferences (key: value)
    ComicAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comic2);

        DBHelper.getInstance(this);

        listView = findViewById(R.id.ac2_listview);

        dataList = new ArrayList<>();
        readData();

        adapter = new ComicAdapter(this, dataList);

        listView.setAdapter(adapter);

        registerForContextMenu(listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Comic comic = dataList.get(position);

                Intent intent = new Intent(Comic2Activity.this, ComicDetailActivity.class);
                intent.putExtra("title", comic.getTitle());
                intent.putExtra("des", comic.getDescription());
                intent.putExtra("thumbnail", comic.getThumbnail());

                startActivity(intent);
            }
        });

        requestPermission();

        //fake data
        BookModify.insert(new Book(0, 100000, "Lap Trinh C", "Tran Van Diep"));
        BookModify.insert(new Book(0, 200000, "Lap Trinh PHP", "Tran Van Diep"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_comic, menu);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_comic_add:
                showMomicDialog(null);
                break;
            case R.id.menu_comic_exit:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_comic_context, menu);

        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        int position = info.position;

        switch (item.getItemId()) {
            case R.id.menu_comic_edit:
                showMomicDialog(dataList.get(position));
                break;
            case R.id.menu_comic_remove:
                dataList.remove(position);
                saveData();

                adapter.notifyDataSetChanged();
                break;
        }

        return super.onContextItemSelected(item);
    }

    void showMomicDialog(final Comic comic) {
        View view = getLayoutInflater().inflate(R.layout.comic_dialog, null);

        final EditText titleTxt = view.findViewById(R.id.cd_title);
        final EditText desTxt = view.findViewById(R.id.cd_description);
        final EditText thumbnailTxt = view.findViewById(R.id.cd_thumbnail);

        if(comic != null) {
            titleTxt.setText(comic.getTitle());
            desTxt.setText(comic.getDescription());
            thumbnailTxt.setText(comic.getThumbnail());
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        builder.setTitle("Comic Editor")
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String title = titleTxt.getText().toString();
                        String des = desTxt.getText().toString();
                        String thumbnail = thumbnailTxt.getText().toString();

                        if(comic != null) {
                            comic.setTitle(title);
                            comic.setThumbnail(thumbnail);
                            comic.setDescription(des);
                        } else {
                            Comic comic2 = new Comic(thumbnail, title, des);
                            dataList.add(comic2);
                        }

                        saveData();

                        adapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
    }

    void saveData() {
        //dataList -> json
        //Type type = new TypeToken<List<Comic>>(){}.getType();
        String json = new Gson().toJson(dataList);

        //SharedPreferences start - C1
        /**
        SharedPreferences sharedPreferences = getSharedPreferences("comics", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("dataList", json);
        editor.putInt("x", 10);
        editor.putLong("y", 100);
        editor.putBoolean("v", true);

        editor.commit();
         */
        //SharedPreferences end
        //C2 - Internal Storage
        /**
        FileOutputStream fos = null;

        try {
            fos = openFileOutput("comics.json", MODE_PRIVATE);

            byte[] data = json.getBytes("utf8");

            fos.write(data);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }*/

        FileOutputStream fos = null;
        try {
            File rootPath = Environment.getExternalStorageDirectory();
            Log.d(Comic2Activity.class.getName(), Environment.DIRECTORY_DOWNLOADS + File.separator + rootPath.getAbsolutePath());
            File file = new File(rootPath, Environment.DIRECTORY_DOWNLOADS + File.separator + "comics.json");
            fos = new FileOutputStream(file);

            byte[] data = json.getBytes("utf8");

            fos.write(data);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    void readData() {
        /** C1
        SharedPreferences sharedPreferences = getSharedPreferences("comics", MODE_PRIVATE);
        String json = sharedPreferences.getString("dataList", "");

        if(!json.isEmpty()) {
            Type type = new TypeToken<List<Comic>>(){}.getType();
            dataList = new Gson().fromJson(json, type);
        }
         */
        //C2 - Internal Storage
        /**FileInputStream fis = null;
        StringBuilder builder = new StringBuilder();

        try {
            fis = openFileInput("comics.json");

            int code;
            while((code = fis.read()) != -1) {
                builder.append((char) code);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }*/
        FileInputStream fis = null;
        StringBuilder builder = new StringBuilder();

        try {
            File rootPath = Environment.getExternalStorageDirectory();
            Log.d(Comic2Activity.class.getName(), Environment.DIRECTORY_DOWNLOADS + File.separator + rootPath.getAbsolutePath());
            File file = new File(rootPath, Environment.DIRECTORY_DOWNLOADS + File.separator + "comics.json");

            fis = new FileInputStream(file);

            int code;
            while((code = fis.read()) != -1) {
                builder.append((char) code);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        //Code Chung - dung vs moi cach.
        String json = builder.toString();
        if(!json.isEmpty()) {
            Type type = new TypeToken<List<Comic>>(){}.getType();
            dataList = new Gson().fromJson(json, type);
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(Comic2Activity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
        } else {
            ActivityCompat.requestPermissions(Comic2Activity.this, new String[] {
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.MANAGE_EXTERNAL_STORAGE
            }, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can use local drive .");
                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                break;
        }
    }
}
