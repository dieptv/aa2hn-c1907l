package com.gokisoft.c1907l.comic.models;

/**
 * Created by Diep.Tran on 9/1/21.
 */

public class Book {
    int _id, price;
    String name, authorName;

    public Book() {
    }

    public Book(int _id, int price, String name, String authorName) {
        this._id = _id;
        this.price = price;
        this.name = name;
        this.authorName = authorName;
    }

    public int getId() {
        return _id;
    }

    public void setId(int _id) {
        this._id = _id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
}
