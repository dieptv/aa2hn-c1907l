package com.gokisoft.c1907l.comic;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.gokisoft.c1907l.R;
import com.gokisoft.c1907l.comic.adapter.ComicSqliteAdapter;
import com.gokisoft.c1907l.comic.db.DBHelper;
import com.gokisoft.c1907l.comic.models.Comic;
import com.gokisoft.c1907l.comic.models.ComicModify;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class ComicSqliteActivity extends AppCompatActivity {
    ListView listView;
    ComicSqliteAdapter adapter;
    Cursor currentCursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comic2);
        DBHelper.getInstance(this);//Chac chan -> khoi tao doi tuong DBHelper 1 lan.

        //Fake data.
//        Comic comic = new Comic("https://static.vecteezy.com/system/resources/thumbnails/000/660/520/small/Laugh_Design.jpg", "Tieu de 1", "Noi dung 1");
//        ComicModify.save(comic);
//        comic = new Comic("https://static.vecteezy.com/system/resources/thumbnails/000/660/520/small/Laugh_Design.jpg", "Tieu de 2", "Noi dung 2");
//        ComicModify.save(comic);

        listView = findViewById(R.id.ac2_listview);

        currentCursor = ComicModify.getComicCursor();

        adapter = new ComicSqliteAdapter(this, currentCursor);

        listView.setAdapter(adapter);

        registerForContextMenu(listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                currentCursor.moveToPosition(position);
                Comic comic = ComicModify.find(currentCursor);

                Intent intent = new Intent(ComicSqliteActivity.this, ComicDetailActivity.class);
                intent.putExtra("title", comic.getTitle());
                intent.putExtra("des", comic.getDescription());
                intent.putExtra("thumbnail", comic.getThumbnail());

                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_comic, menu);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_comic_add:
                showMomicDialog(null);
                break;
            case R.id.menu_comic_exit:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_comic_context, menu);

        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        int position = info.position;
        currentCursor.moveToPosition(position);
        Comic comic = ComicModify.find(currentCursor);

        switch (item.getItemId()) {
            case R.id.menu_comic_edit:
                showMomicDialog(comic);
                break;
            case R.id.menu_comic_remove:
                ComicModify.delete(comic.getId());

                currentCursor = ComicModify.getComicCursor();
                adapter.changeCursor(currentCursor);
                adapter.notifyDataSetChanged();
                break;
        }

        return super.onContextItemSelected(item);
    }

    void showMomicDialog(final Comic comic) {
        View view = getLayoutInflater().inflate(R.layout.comic_dialog, null);

        final EditText titleTxt = view.findViewById(R.id.cd_title);
        final EditText desTxt = view.findViewById(R.id.cd_description);
        final EditText thumbnailTxt = view.findViewById(R.id.cd_thumbnail);

        if(comic != null) {
            titleTxt.setText(comic.getTitle());
            desTxt.setText(comic.getDescription());
            thumbnailTxt.setText(comic.getThumbnail());
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        builder.setTitle("Comic Editor")
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String title = titleTxt.getText().toString();
                        String des = desTxt.getText().toString();
                        String thumbnail = thumbnailTxt.getText().toString();

                        if(comic != null) {
                            comic.setTitle(title);
                            comic.setThumbnail(thumbnail);
                            comic.setDescription(des);
                            ComicModify.save(comic);
                        } else {
                            Comic comic2 = new Comic(thumbnail, title, des);
                            ComicModify.save(comic2);
                        }

                        currentCursor = ComicModify.getComicCursor();
                        adapter.changeCursor(currentCursor);
                        adapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
    }
}
