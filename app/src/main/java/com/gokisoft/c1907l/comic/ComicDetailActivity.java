package com.gokisoft.c1907l.comic;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.gokisoft.c1907l.R;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AppCompatActivity;

public class ComicDetailActivity extends AppCompatActivity {
    TextView titleView, desView;
    ImageView thumbnailView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comic_detail);

        titleView = findViewById(R.id.acd_title);
        desView = findViewById(R.id.acd_description);
        thumbnailView = findViewById(R.id.acd_thumbnail);

        String title = getIntent().getStringExtra("title");
        String des = getIntent().getStringExtra("des");
        String thumbnail = getIntent().getStringExtra("thumbnail");

        titleView.setText(title);
        desView.setText(des);
        Picasso.with(this).load(thumbnail).into(thumbnailView);
    }
}
