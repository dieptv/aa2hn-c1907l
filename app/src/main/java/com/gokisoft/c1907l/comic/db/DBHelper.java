package com.gokisoft.c1907l.comic.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.gokisoft.c1907l.comic.models.BookModify;
import com.gokisoft.c1907l.comic.models.ComicModify;

/**
 * Created by Diep.Tran on 8/30/21.
 */

public class DBHelper extends SQLiteOpenHelper{
    private static final String DB_NAME = "C1907L";
    private static final int VERSION = 3;

    private static DBHelper instance = null;

    private DBHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    public synchronized static DBHelper getInstance(Context context) {
        if(instance == null) {
            instance = new DBHelper(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Khi 1 user -> cai dat moi ung dung -> chay vao method duy nhat 1 lan.
        //Tao table trong database.
        //VERSION 1
        db.execSQL(ComicModify.SQL_CREATE_TABLE);
        //VERSION 2
        db.execSQL(BookModify.SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Nguoi da cai ung dung truoc do.
        //Nang cap phien ban VERSION -> dc goi vao day 1 lan duy nhat.
        //oldVersion -> 1, 2, 3, 4, 5 -> chuc nang moi -> upgrade
        if(oldVersion == 1) {
            db.execSQL(BookModify.SQL_CREATE_TABLE);
            //update 2 - C1
        }
        if(oldVersion < 2) {
            //update 2 - C2
        }
        if(oldVersion < 3) {
            //update 3
        }
        if(oldVersion < 4) {
            //update 4
        }
        if(oldVersion < 5) {
            //update 5
        }
    }
}
