package com.gokisoft.c1907l.comic.models;

/**
 * Created by Diep.Tran on 8/25/21.
 */

public class Comic {
    int _id;
    String thumbnail;
    String title, description;

    public Comic() {
    }

    public Comic(String thumbnail, String title, String description) {
        this.thumbnail = thumbnail;
        this.title = title;
        this.description = description;
    }

    public Comic(int _id, String title, String thumbnail, String description) {
        this._id = _id;
        this.thumbnail = thumbnail;
        this.title = title;
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return _id;
    }

    public void setId(int _id) {
        this._id = _id;
    }
}
