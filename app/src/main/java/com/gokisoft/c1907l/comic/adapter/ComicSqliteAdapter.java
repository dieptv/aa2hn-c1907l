package com.gokisoft.c1907l.comic.adapter;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gokisoft.c1907l.R;
import com.gokisoft.c1907l.comic.models.Comic;
import com.gokisoft.c1907l.comic.models.ComicModify;
import com.squareup.picasso.Picasso;

/**
 * Created by Diep.Tran on 8/30/21.
 */

public class ComicSqliteAdapter extends CursorAdapter{
    Activity activity;

    public ComicSqliteAdapter(Activity activity, Cursor c) {
        super(activity, c);
        this.activity = activity;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return activity.getLayoutInflater().inflate(R.layout.comic_item, null);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ImageView thumbnailView = view.findViewById(R.id.ci_thumbnail);
        TextView titleView = view.findViewById(R.id.ci_title);
        TextView desView = view.findViewById(R.id.ci_description);

        Comic comic = ComicModify.find(cursor);

        titleView.setText(comic.getTitle());
        desView.setText(comic.getDescription());
        Picasso.with(activity).load(comic.getThumbnail()).into(thumbnailView);
    }
}
