package com.gokisoft.c1907l;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.gokisoft.c1907l.config.Config;

import androidx.appcompat.app.AppCompatActivity;

public class EditorSubjectActivity extends AppCompatActivity implements View.OnClickListener{
    EditText titleTxt;
    Button saveBtn, exitBtn;

    int position = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject_editor);

        titleTxt = findViewById(R.id.ase_subject_name);
        saveBtn = findViewById(R.id.ase_save_btn);
        exitBtn = findViewById(R.id.ase_exit_btn);

        String subjectName = getIntent().getStringExtra("subject_name");
        position = getIntent().getIntExtra("position", -1);

        titleTxt.setText(subjectName);

        saveBtn.setOnClickListener(this);
        exitBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.equals(exitBtn)) {
            finish();
        } else if(v.equals(saveBtn)) {
            //Day du lieu ve Subject Activity
            Intent i = new Intent();
            i.putExtra("subject_name", titleTxt.getText().toString());
            i.putExtra("position", position);
            setResult(Config.REQUEST_CODE_EDITOR_SUBJECT, i);

            finish();
        }
    }
}
