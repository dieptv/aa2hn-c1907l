package com.gokisoft.c1907l;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.gokisoft.c1907l.models.Book;
import com.gokisoft.c1907l.models.DataController;

import androidx.appcompat.app.AppCompatActivity;

public class LogicBookViewActivity extends AppCompatActivity implements View.OnClickListener{
    TextView bookNameView, authorNameView, priceView, desView;
    Button updateBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_view);

        //Mapping
        bookNameView = findViewById(R.id.abv_book_name);
        authorNameView = findViewById(R.id.abv_author_name);
        priceView = findViewById(R.id.abv_price);
        desView = findViewById(R.id.abv_description);

        updateBtn = findViewById(R.id.abv_update);

        updateBtn.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_book, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_book_exit:
                Toast.makeText(this, "Ket thuc chuong trinh", Toast.LENGTH_SHORT).show();
                finish();
                break;
            case R.id.menu_book_update:
                Intent i = new Intent(this, LogicMainActivity.class);
                startActivity(i);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

//        MyApplication myApp = (MyApplication) getApplicationContext();
//        Book book = myApp.getDataController().getBook();
        Book book = DataController.getInstance().getBook();
        Log.d(LogicBookViewActivity.class.getName(), book.toString());
        bookNameView.setText(book.getBookName());
        authorNameView.setText(book.getAuthorName());
        priceView.setText(book.getPrice()+"");
        desView.setText(book.getDescription());
    }

    @Override
    public void onClick(View v) {
        if(v.equals(updateBtn)) {
            //Khi nguoi dung click button update
            Intent i = new Intent(this, LogicMainActivity.class);
            startActivity(i);
        }
    }
}
