package com.gokisoft.c1907l;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.gokisoft.c1907l.config.Config;

import androidx.appcompat.app.AppCompatActivity;

public class EditorComicActivity extends AppCompatActivity implements View.OnClickListener{
    EditText titleTxt, thumbnailTxt, desTxt;
    Button exitBtn, saveBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor_comic);

        titleTxt = findViewById(R.id.aec_title);
        thumbnailTxt = findViewById(R.id.aec_description);
        desTxt = findViewById(R.id.aec_thumbnail);

        exitBtn = findViewById(R.id.aec_exit);
        saveBtn = findViewById(R.id.aec_save);

        exitBtn.setOnClickListener(this);
        saveBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.equals(exitBtn)) {
            finish();
        } else if(v.equals(saveBtn)) {
            //Gui du lieu ve ComicActivity -> broadcast
            //sender
            Intent i = new Intent();
            i.setAction(Config.ACTION_COMIC_EDITOR);
            i.putExtra("title", titleTxt.getText().toString());
            i.putExtra("description", desTxt.getText().toString());
            i.putExtra("thumbnail", thumbnailTxt.getText().toString());

            sendBroadcast(i);

            finish();
        }
    }
}
