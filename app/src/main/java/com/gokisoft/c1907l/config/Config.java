package com.gokisoft.c1907l.config;

/**
 * Created by Diep.Tran on 8/20/21.
 */

public interface Config {
    int REQUEST_CODE_UPDATE_BOOK = 1;//public static final int -> int
    int REQUEST_CODE_EDITOR_SUBJECT = 2;

    String ACTION_COMIC_EDITOR = "ACTION_COMIC_EDITOR";
    String ACTION_SEND_PERCENT = "ACTION_SEND_PERCENT";

    String YOUTUBE_URL = "https://r3---sn-8pxuuxa-i5ok.googlevideo.com/videoplayback?expire=1631282636&ei=bBE7YfyeNNuO1gKFw6L4Cg&ip=5.105.211.167&id=o-AD9ZIeWLPr6w_ENIrl6P4pB8BcYUANDPB_Flw42FibEw&itag=22&source=youtube&requiressl=yes&vprv=1&mime=video%2Fmp4&ns=6iNJHgXpRh3E1bUugI2A1qIG&ratebypass=yes&dur=2249.549&lmt=1628123412477803&fexp=24001373,24007246&c=WEB&txp=5532434&n=B3W-Zw3bU9pYUPEb&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRgIhALTW-gzqQRfalLzrwiETf1qK4eHzDpdyp5X3l92CJQueAiEAxxDWjydDjmLNPsE2ZLwezkvaWcYgEC2KUn65-boINKE%3D&title=Wheels%20on%20the%20Bus%20(Play%20Version)%20%2B%20More%20Nursery%20Rhymes%20%26%20Kids%20Songs%20-%20CoComelon&rm=sn-ugpoput-h2fe7s,sn-3c2eyel&req_id=7857c5e56f92a3ee&redirect_counter=2&cms_redirect=yes&ipbypass=yes&mh=hb&mip=171.224.178.84&mm=29&mn=sn-8pxuuxa-i5ok&ms=rdu&mt=1631275490&mv=m&mvi=3&pl=24&lsparams=ipbypass,mh,mip,mm,mn,ms,mv,mvi,pl&lsig=AG3C_xAwRQIgRMZkM8JsIYXrx8oJ2qimsAjK-8gwIzfNBIbO3Fu1uR4CIQClbWzjOJV_OFzVvcedRNZV8EsxuD8NDE8MX4kXfifDFQ%3D%3D";
    String YOUTUBE_URL2 = "https://r2---sn-8pxuuxa-i5oel.googlevideo.com/videoplayback?expire=1631297380&ei=BEs7YcWXHNGYgAfcybHoDQ&ip=91.90.123.71&id=o-AF0i9GsdtiwvK0M78W-G_AywWF4g3x-bXJcDNIoUK9lG&itag=22&source=youtube&requiressl=yes&vprv=1&mime=video%2Fmp4&ns=taIWDgZ8GQ4UmXAMppYUMPIG&cnr=14&ratebypass=yes&dur=959.820&lmt=1630527223317371&fexp=24001373,24007246&c=WEB&txp=5532434&n=tSpG_VOe6-Dggr8H&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Ccnr%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRAIgOmEjt-8g93QKPQEuRgxFO2FZisO1uQT1bbBHUbT-dGYCIDh-ej-8etCyH6UxAmnfnouttqoxo8RMjDlgAdtEESqm&title=C%C3%A1%20m%E1%BA%ADp%20con%20%7C%20Ca%20nh%E1%BA%A1c%20thi%E1%BA%BFu%20nhi%20%7C%20V%E1%BA%A7n%20%C4%91i%E1%BB%87u%20tr%E1%BA%BB%20%7C%20Little%20Treehouse%20Vietnam%20%7C%20Ho%E1%BA%A1t%20H%C3%ACnh&redirect_counter=1&rm=sn-5hness7l&req_id=c680dc774487a3ee&cms_redirect=yes&ipbypass=yes&mh=Fz&mip=171.224.178.84&mm=31&mn=sn-8pxuuxa-i5oel&ms=au&mt=1631275493&mv=m&mvi=2&pl=24&lsparams=ipbypass,mh,mip,mm,mn,ms,mv,mvi,pl&lsig=AG3C_xAwRAIgZoZcjKm_FcK2ZdWUommOSbKasGg6-OdAdMkHAxl4ficCIEliae8F9SX9rI0bWuQQ6HxivcCaKMxYOIB2KRapfm3A";
}
