package com.gokisoft.c1907l.bt2113;

import android.os.Bundle;
import android.widget.TextView;

import com.gokisoft.c1907l.R;

import androidx.appcompat.app.AppCompatActivity;

public class MusicDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_detail);

        String title = getIntent().getStringExtra("title");
        String des = getIntent().getStringExtra("description");
        String createdAt = getIntent().getStringExtra("created_at");

        TextView titleView = findViewById(R.id.amd_title);
        TextView desView = findViewById(R.id.amd_description);
        TextView createdAtView = findViewById(R.id.amd_created_at);

        titleView.setText(title);
        desView.setText(des);
        createdAtView.setText(createdAt);
    }
}
