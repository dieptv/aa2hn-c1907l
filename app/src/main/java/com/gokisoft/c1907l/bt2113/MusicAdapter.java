package com.gokisoft.c1907l.bt2113;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.gokisoft.c1907l.R;

/**
 * Created by Diep.Tran on 9/17/21.
 */

public class MusicAdapter extends CursorAdapter{
    Activity activity;

    public MusicAdapter(Activity activity, Cursor c) {
        super(activity, c);
        this.activity = activity;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return activity.getLayoutInflater().inflate(R.layout.music_item, null);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView titleView = view.findViewById(R.id.mi_title);
        TextView desView = view.findViewById(R.id.mi_description);
        TextView dateView = view.findViewById(R.id.mi_created_at);

        Music music = MusicModify.getMusicFromCursor(cursor);

        titleView.setText(music.getTitle());
        desView.setText(music.getDescription());
        dateView.setText(music.convertDateToString());
    }
}
