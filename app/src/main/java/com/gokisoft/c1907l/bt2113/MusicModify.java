package com.gokisoft.c1907l.bt2113;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Diep.Tran on 9/17/21.
 */

public class MusicModify {
    static final String TABLE_NAME = "music";

    public static final String QUERY_CREATE_TABLE = "create table music (\n" +
            "\t_id integer primary key autoincrement,\n" +
            "\ttitle varchar(200),\n" +
            "\tdescription text,\n" +
            "\tcreated_at date\n" +
            ")";

    public static void insert(Music music) {
        SQLiteDatabase db = DBHelper.getInstance(null).getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("title", music.getTitle());
        values.put("description", music.getDescription());
        values.put("created_at", music.convertDateToString());

        db.insert(TABLE_NAME, null, values);
    }

    public static void update(Music music) {
        SQLiteDatabase db = DBHelper.getInstance(null).getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("title", music.getTitle());
        values.put("description", music.getDescription());

        db.update(TABLE_NAME, values, "_id = " + music.getId(), null);
    }

    public static void delete(int id) {
        SQLiteDatabase db = DBHelper.getInstance(null).getWritableDatabase();

        db.delete(TABLE_NAME, "_id = " + id, null);
    }

    public static Cursor getMusicCursor() {
        SQLiteDatabase db = DBHelper.getInstance(null).getReadableDatabase();

        String sql = "select * from " + TABLE_NAME;

        Cursor cursor = db.rawQuery(sql, null);

        return cursor;
    }

    public static Music getMusicFromCursor(Cursor cursor) {
        Music music = new Music(
                cursor.getInt(cursor.getColumnIndex("_id")),
                cursor.getString(cursor.getColumnIndex("title")),
                cursor.getString(cursor.getColumnIndex("description")),
                cursor.getString(cursor.getColumnIndex("created_at"))
        );
        return music;
    }
}
