package com.gokisoft.c1907l.bt2113;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.gokisoft.c1907l.R;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class MusicActivity extends AppCompatActivity {
    ListView listView;
    Cursor currentCursor;
    MusicAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);

        //Khoi tao database + tables -> init singleton.
        DBHelper.getInstance(this);

        //Fake
//        Music music1 = new Music("Bai hat 1", "Noi dung 1");
//        MusicModify.insert(music1);
//        Music music2 = new Music("Bai hat 2", "Noi dung 2");
//        MusicModify.insert(music2);

        listView = findViewById(R.id.am_listview);
        currentCursor = MusicModify.getMusicCursor();
        adapter = new MusicAdapter(this, currentCursor);

        listView.setAdapter(adapter);

        registerForContextMenu(listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                currentCursor.moveToPosition(position);
                Music music = MusicModify.getMusicFromCursor(currentCursor);

                Intent i = new Intent(MusicActivity.this, MusicDetailActivity.class);
                i.putExtra("title", music.getTitle());
                i.putExtra("description", music.getDescription());
                i.putExtra("created_at", music.convertDateToString());

                startActivity(i);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_music, menu);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_music_add:
                showMusicDialog(null);
                break;
            case R.id.menu_music_exit:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_music_context, menu);

        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        int position = info.position;
        currentCursor.moveToPosition(position);
        Music music = MusicModify.getMusicFromCursor(currentCursor);

        switch (item.getItemId()) {
            case R.id.menu_music_edit:
                showMusicDialog(music);
                break;
            case R.id.menu_music_remove:
                MusicModify.delete(music.getId());

                currentCursor = MusicModify.getMusicCursor();
                adapter.changeCursor(currentCursor);
                adapter.notifyDataSetChanged();
                break;
        }

        return super.onContextItemSelected(item);
    }

    private void showMusicDialog(final Music music) {
        View view = getLayoutInflater().inflate(R.layout.music_dialog, null);
        final TextView titleTxt = view.findViewById(R.id.md_title);
        final TextView desTxt = view.findViewById(R.id.md_description);

        if(music != null) {
            titleTxt.setText(music.getTitle());
            desTxt.setText(music.getDescription());
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);

        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(music != null) {
                    music.setTitle(titleTxt.getText().toString());
                    music.setDescription(desTxt.getText().toString());

                    MusicModify.update(music);
                } else {
                    Music m = new Music(titleTxt.getText().toString(), desTxt.getText().toString());
                    MusicModify.insert(m);
                }

                currentCursor = MusicModify.getMusicCursor();
                adapter.changeCursor(currentCursor);
                adapter.notifyDataSetChanged();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        }).show();
    }
}
