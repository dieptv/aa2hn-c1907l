package com.gokisoft.c1907l.bt2113;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Diep.Tran on 9/17/21.
 */

public class DBHelper extends SQLiteOpenHelper{
    static final String DBNAME = "MusicDB";
    static final int VERSION = 1;

    static DBHelper instance = null;

    private DBHelper(Context context) {
        super(context, DBNAME, null, VERSION);
    }

    public synchronized static DBHelper getInstance(Context context) {
        if(instance == null) {
            instance = new DBHelper(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(MusicModify.QUERY_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
