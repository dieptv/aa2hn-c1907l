package com.gokisoft.c1907l;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class BookDetailActivity extends AppCompatActivity {
    TextView bookNameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_detail);

        String bookName = getIntent().getStringExtra("bookName");

        bookNameView = findViewById(R.id.abd_book_name);
        bookNameView.setText(bookName);
    }
}
