package com.gokisoft.c1907l.app;

import android.app.Application;

import com.gokisoft.c1907l.comic.db.DBHelper;
import com.gokisoft.c1907l.models.DataController;

/**
 * Created by Diep.Tran on 8/20/21.
 */

public class MyApplication extends Application{
    DataController dataController;

    @Override
    public void onCreate() {
        dataController = DataController.getInstance();
        DBHelper.getInstance(getApplicationContext());
        super.onCreate();
    }

    public DataController getDataController() {
        return dataController;
    }

    public void setDataController(DataController dataController) {
        this.dataController = dataController;
    }
}
