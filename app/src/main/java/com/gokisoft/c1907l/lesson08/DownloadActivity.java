package com.gokisoft.c1907l.lesson08;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gokisoft.c1907l.R;
import com.gokisoft.c1907l.config.Config;

import androidx.appcompat.app.AppCompatActivity;

public class DownloadActivity extends AppCompatActivity implements View.OnClickListener{
    Button startServiceBtn, stopServiceBtn;
    ProgressBar progressBar;
    TextView titleView;
    Intent serviceIntent = null;

    BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            switch (action) {
                case Config.ACTION_SEND_PERCENT:
                    final int percent = intent.getIntExtra("percent", 0);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(percent <= 100) {
                                progressBar.setProgress(percent);
                                titleView.setText("Loading "+percent+"% ...");
                            }

                            if(percent >= 100) {
                                onClick(stopServiceBtn);
                            }
                        }
                    });
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);

        startServiceBtn = findViewById(R.id.ad_start_service_btn);
        stopServiceBtn = findViewById(R.id.ad_stop_service_btn);
        progressBar = findViewById(R.id.ad_progressbar);
        titleView = findViewById(R.id.ad_title);

        startServiceBtn.setOnClickListener(this);
        stopServiceBtn.setOnClickListener(this);

        try {
            IntentFilter filter = new IntentFilter();
            filter.addAction(Config.ACTION_SEND_PERCENT);
            registerReceiver(myReceiver, filter);
        } catch(Exception e) {}

        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.sample_animation);
        startServiceBtn.startAnimation(animation);
    }

    @Override
    public void onClick(View v) {
        if(v.equals(startServiceBtn)) {
            serviceIntent = new Intent(this, DownloadService.class);
            serviceIntent.putExtra("video_url", Config.YOUTUBE_URL);
            startService(serviceIntent);
            startServiceBtn.setVisibility(View.GONE);
            stopServiceBtn.setVisibility(View.VISIBLE);
        } else if(v.equals(stopServiceBtn)) {
            stopService(serviceIntent);
            startServiceBtn.setVisibility(View.VISIBLE);
            stopServiceBtn.setVisibility(View.GONE);
        }
    }

    @Override
    public void finish() {
        try {
            onClick(stopServiceBtn);
            unregisterReceiver(myReceiver);
            myReceiver = null;
        } catch(Exception e) {}
        super.finish();
    }
}
