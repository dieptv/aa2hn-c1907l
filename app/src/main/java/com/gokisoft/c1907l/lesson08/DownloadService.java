package com.gokisoft.c1907l.lesson08;

import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.gokisoft.c1907l.config.Config;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class DownloadService extends Service {
    int percent = 0;
    boolean isLive = true;
    Handler handler;
    String videoUrl;

    public DownloadService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(DownloadService.class.getName(), "onBind ...");

        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(DownloadService.class.getName(), "onCreate ...");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(DownloadService.class.getName(), "onStartCommand ...");
        //C1: Chuong trinh TEST
//        handler = new Handler();
//        sendData();
        videoUrl = intent.getStringExtra("video_url");
        Log.d(DownloadService.class.getName(), videoUrl);
        new Thread(new Runnable() {
            @Override
            public void run() {
                downloadFile();
            }
        }).start();

        return super.onStartCommand(intent, flags, startId);
    }

    private void downloadFile() {
        InputStream input = null;
        OutputStream output = null;

        int count;
        try {
            URL url = new URL(videoUrl);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream
            output = new FileOutputStream(Environment
                    .getExternalStorageDirectory().toString()
                    + File.separator
                    + Environment.DIRECTORY_MOVIES
                    + File.separator
                    + "youtube.mp4");

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                if(!isLive) {
                    break;
                }
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                int percent = (int) ((total * 100) / lenghtOfFile);

                Intent i = new Intent();
                i.setAction(Config.ACTION_SEND_PERCENT);
                i.putExtra("percent", percent);

                sendBroadcast(i);

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // closing streams
            if(output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void sendData() {
        if(!isLive) {
            return;
        }

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent();
                i.setAction(Config.ACTION_SEND_PERCENT);
                i.putExtra("percent", ++percent);

                sendBroadcast(i);

                sendData();
            }
        }, 300);
    }

    @Override
    public void onDestroy() {
        Log.d(DownloadService.class.getName(), "onDestroy ...");
        isLive = false;
        super.onDestroy();
    }
}
