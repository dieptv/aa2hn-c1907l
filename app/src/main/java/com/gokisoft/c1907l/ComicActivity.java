package com.gokisoft.c1907l;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;

import com.gokisoft.c1907l.adapter.OptimiseComicAdapter;
import com.gokisoft.c1907l.config.Config;
import com.gokisoft.c1907l.models.Comic;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;

public class ComicActivity extends AppCompatActivity {
    ListView listView;
    List<Comic> dataList;
    OptimiseComicAdapter adapter;

    BroadcastReceiver comicReceiver = new  BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //Noi nhan du lieu.
            String action = intent.getAction();

            switch (action) {
                case Config.ACTION_COMIC_EDITOR:
                    Comic c = new Comic(
                            intent.getStringExtra("thumbnail"),
                            intent.getStringExtra("title"),
                            intent.getStringExtra("description")
                    );
                    dataList.add(c);

                    adapter.notifyDataSetChanged();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comic);

        listView = findViewById(R.id.ca_listview);

        //Fake du lieu
        dataList = new ArrayList<>();
        dataList.add(new Comic("https://i.chzbgr.com/full/9175388928/hCC2E3E2C/funny-comic-cartoon-choose-i-choose-you-your-first-pokemon-oak-oak-sephko", "Truyen 1", "Noi Dung 1"));
        dataList.add(new Comic("https://static.vecteezy.com/system/resources/thumbnails/000/660/520/small/Laugh_Design.jpg", "Truyen 2", "Noi Dung 2"));
        dataList.add(new Comic("https://i.pinimg.com/originals/52/ee/43/52ee437272c57b92351eda4156eca703.jpg", "Truyen 3", "Noi Dung 3"));

        //Tao adapter
        adapter = new OptimiseComicAdapter(this, dataList);

        listView.setAdapter(adapter);

        //Khoi tao du lieu.
        IntentFilter filter = new IntentFilter();
        filter.addAction(Config.ACTION_COMIC_EDITOR);

        //Dang ky lang nghe
        try {
            registerReceiver(comicReceiver, filter);
        } catch(Exception e) {}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_subject, menu);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_subject_add:
                Intent i = new Intent(this, EditorComicActivity.class);
                startActivity(i);
                break;
            case R.id.menu_subject_exit:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void finish() {
        try {
            unregisterReceiver(comicReceiver);
        } catch(Exception e) {}

        super.finish();
    }
}
