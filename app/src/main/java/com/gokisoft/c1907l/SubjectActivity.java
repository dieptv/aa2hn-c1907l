package com.gokisoft.c1907l;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.gokisoft.c1907l.config.Config;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class SubjectActivity extends AppCompatActivity {
    ListView listView;
    List<String> dataList;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject);

        //Mapping UI
        listView = findViewById(R.id.as_listview);

        //Khoi tao data
        dataList = new ArrayList<>();

        dataList.add("Lap Trinh C");
        dataList.add("Lap Trinh HTML");
        dataList.add("Lap Trinh CSS");
        dataList.add("Lap Trinh JS");
        dataList.add("Lap Trinh jQuery");
        dataList.add("Lap Trinh SQL Server");
        dataList.add("Lap Trinh PHP");
        dataList.add("Lap Trinh Laravel");
        dataList.add("Lap Trinh Java");

        //Khoi tao adapter
        adapter = new ArrayAdapter<String>(this,
                R.layout.subject_item, R.id.si_title, dataList);

        //Cai dat adapter vao ListView
        listView.setAdapter(adapter);

        //Bat su kien khi click vao item
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String v = dataList.get(position);
                Toast.makeText(SubjectActivity.this, v, Toast.LENGTH_SHORT).show();
            }
        });

        //Dang ky context menu
        registerForContextMenu(listView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_subject, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_subject_context, menu);

        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.menu_subject_edit:
//                Intent i = new Intent(this, EditorSubjectActivity.class);
//                i.putExtra("subject_name", dataList.get(info.position));
//                i.putExtra("position", info.position);
//                startActivityForResult(i, Config.REQUEST_CODE_EDITOR_SUBJECT);
                showSubjectDialog(info.position);
                break;
            case R.id.menu_subject_remove:
                dataList.remove(info.position);
                adapter.notifyDataSetChanged();
                break;
        }

        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_subject_add:
//                Intent i = new Intent(this, EditorSubjectActivity.class);
//                i.putExtra("subject_name", "");
//                i.putExtra("position", -1);
//                startActivityForResult(i, Config.REQUEST_CODE_EDITOR_SUBJECT);
                showSubjectDialog(-1);
                break;
            case R.id.menu_subject_exit:
//                finish();
                showConfirmDialog();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case Config.REQUEST_CODE_EDITOR_SUBJECT:
                String subjectName = data.getStringExtra("subject_name");
                int position = data.getIntExtra("position", -1);
                //Cap nhat len ListView
                if(position >= 0) {
                    dataList.set(position, subjectName);
                } else {
                    dataList.add(subjectName);
                }

                adapter.notifyDataSetChanged();
                break;
        }
    }

    void showConfirmDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Thong Bao")
                .setMessage("Ban co chac chan muon thoat chuong trinh khong?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("Huy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
    }

    void showSubjectDialog(final int position) {
        //Load view from layout
        View v = getLayoutInflater().inflate(R.layout.subject_dialog, null);

        final EditText subjectTxt = v.findViewById(R.id.sd_subject_name);
        if(position >= 0) {
            subjectTxt.setText(dataList.get(position));
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(v);

        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String subjectName = subjectTxt.getText().toString();
                if(position >= 0) {
                    dataList.set(position, subjectName);
                    adapter.notifyDataSetChanged();
                } else {
                    dataList.add(subjectName);
                    adapter.notifyDataSetChanged();
                }
            }
        }).setNegativeButton("Huy", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        }).show();
    }
}
