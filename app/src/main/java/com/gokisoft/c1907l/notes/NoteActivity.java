package com.gokisoft.c1907l.notes;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.gokisoft.c1907l.R;
import com.gokisoft.c1907l.notes.adapter.NoteAdapter;
import com.gokisoft.c1907l.notes.models.Message;
import com.gokisoft.c1907l.notes.models.Note;
import com.gokisoft.c1907l.notes.models.NoteService;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NoteActivity extends Activity {
    static final String BASE_URL = "http://192.168.2.2/notes/";

    ListView listView;
    List<Note> dataList = new ArrayList<>();
    NoteAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        listView = findViewById(R.id.an_listview);

        adapter = new NoteAdapter(this, dataList);
        listView.setAdapter(adapter);

        loadNotes();

        addNote();
    }

    void addNote() {
        Note note = new Note();
        note.title = "Xin chao";
        note.description = "Noi dung";
        note.status = 0;
        note.action = "add";

        OkHttpClient client = new OkHttpClient.Builder().build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        NoteService noteService = retrofit.create(NoteService.class);
        Call<Message> callAsync = noteService.doActionNote(note.action, note.title, note.description, note.status);
        callAsync.enqueue(new Callback<Message>() {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response) {
                if (response.isSuccessful()) {
                    final Message apiResponse = response.body();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(NoteActivity.this, apiResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    System.out.println("Request Error :: " + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<Message> call, Throwable t) {
                if (call.isCanceled()) {
                    System.out.println("Call was cancelled forcefully");
                } else {
                    System.out.println("Network Error :: " + t.getLocalizedMessage());
                }
            }
        });
    }

    void loadNotes() {
        OkHttpClient client = new OkHttpClient.Builder().build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        NoteService noteService = retrofit.create(NoteService.class);
        Call<List<Note>> callAsync = noteService.getNoteList();
        callAsync.enqueue(new Callback<List<Note>>() {
            @Override
            public void onResponse(Call<List<Note>> call, Response<List<Note>> response) {
                if (response.isSuccessful()) {
                    List<Note> apiResponse = response.body();

                    for (Note n : apiResponse) {
                        dataList.add(n);
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });
                } else {
                    System.out.println("Request Error :: " + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<List<Note>> call, Throwable t) {
                if (call.isCanceled()) {
                    System.out.println("Call was cancelled forcefully");
                } else {
                    System.out.println("Network Error :: " + t.getLocalizedMessage());
                }
            }
        });
    }
}
