package com.gokisoft.c1907l.notes.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Diep.Tran on 9/15/21.
 */

public class Message {
    @SerializedName("status")
    public int status;

    @SerializedName("msg")
    public String message;
}
