package com.gokisoft.c1907l.notes.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gokisoft.c1907l.R;
import com.gokisoft.c1907l.notes.models.Note;

import java.util.List;

/**
 * Created by Diep.Tran on 9/15/21.
 */

public class NoteAdapter extends BaseAdapter{
    Activity activity;
    List<Note> dataList;

    public NoteAdapter(Activity activity, List<Note> dataList) {
        this.activity = activity;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = activity.getLayoutInflater().inflate(R.layout.note_item, null);
        }
        TextView titleView = convertView.findViewById(R.id.ni_title);
        TextView desView = convertView.findViewById(R.id.ni_description);
        TextView updatedAtView = convertView.findViewById(R.id.ni_updated_at);
        LinearLayout bgStatus = convertView.findViewById(R.id.ni_status);

        Note note = dataList.get(position);

        titleView.setText(note.title);
        desView.setText(note.description);
        updatedAtView.setText(note.updatedAt);
        if(note.status == 1) {
            bgStatus.setBackgroundColor(Color.RED);
        } else {
            bgStatus.setBackgroundColor(Color.YELLOW);
        }

        return convertView;
    }
}
