package com.gokisoft.c1907l.notes.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Diep.Tran on 9/15/21.
 */

public class Note {
    @SerializedName("id")
    public int id;

    public String action;

    @SerializedName("title")
    public String title;

    @SerializedName("description")
    public String description;

    @SerializedName("updated_at")
    public String updatedAt;

    @SerializedName("status")
    public int status;
}
