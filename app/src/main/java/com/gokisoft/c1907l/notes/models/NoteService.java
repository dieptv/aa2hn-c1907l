package com.gokisoft.c1907l.notes.models;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Diep.Tran on 9/15/21.
 */

public interface NoteService {
    @GET("api/notes/list.php")
    Call<List<Note>> getNoteList();

    @POST("api/notes/post.php")
    @FormUrlEncoded
    Call<Message> doActionNote(@Field("action") String action,
                               @Field("title") String title,
                               @Field("desciption") String desciption,
                               @Field("status") int status);
}
