package com.gokisoft.c1907l;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.gokisoft.c1907l.models.Book;
import com.gokisoft.c1907l.models.DataController;

import androidx.appcompat.app.AppCompatActivity;

public class LogicMainActivity extends AppCompatActivity {
    EditText bookNameTxt, authorNameTxt, priceTxt, descriptionTxt;
    Button saveBtn, resetBtn, bookDetailBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Mapping UI -> Object trong Java
        bookNameTxt = findViewById(R.id.am_book_name);
        authorNameTxt = findViewById(R.id.am_author_name);
        priceTxt = findViewById(R.id.am_price);
        descriptionTxt = findViewById(R.id.am_description);
        saveBtn = findViewById(R.id.am_save_btn);
        resetBtn = findViewById(R.id.am_reset_btn);
        bookDetailBtn = findViewById(R.id.am_book_detail_btn);

        String bookName = getIntent().getStringExtra("bookName");
        String authorName = getIntent().getStringExtra("authorName");
        int price = getIntent().getIntExtra("price", 0);
        String description = getIntent().getStringExtra("description");

        bookNameTxt.setText(bookName);
        authorNameTxt.setText(authorName);
        descriptionTxt.setText(description);
        priceTxt.setText(""+price);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Khi nguoi dung click day -> xu ly.
                Log.d(MainActivity.class.getName(), "click save ...");
                String bookName = bookNameTxt.getText().toString();
                String authorName = authorNameTxt.getText().toString();
                int price = Integer.parseInt(priceTxt.getText().toString());
                String description = descriptionTxt.getText().toString();

//                MyApplication myApp = (MyApplication) getApplicationContext();
//                Book book = myApp.getDataController().getBook();
                Book book = DataController.getInstance().getBook();
                book.setBookName(bookName);
                book.setAuthorName(authorName);
                book.setPrice(price);
                book.setDescription(description);
                Log.d(LogicBookViewActivity.class.getName(), book.toString());

                finish();
            }
        });

        resetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookNameTxt.setText("");
                authorNameTxt.setText("");
                priceTxt.setText("");
                descriptionTxt.setText("");
            }
        });
    }
}
