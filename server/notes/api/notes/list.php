<?php
require_once ('../../db/dbhelper.php');
require_once ('../../utils/utility.php');

$sql = "select * from notes";

$result = executeResult($sql);

$res = [];

foreach ($result as $item) {
	$mydate = new \DateTime($item['updated_at']);
	$mydate->modify('+7 hours');
	$mydate = $mydate->format('H:i d/m/Y');

	$res[] = [
		"id"          => $item["id"],
		"title"       => $item["title"],
		"description" => $item["description"],
		"status"      => $item["status"],
		"updated_at"  => $mydate
	];
}

echo json_encode($res);